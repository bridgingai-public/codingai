# Basics of Machine Learning - Exercises

This repository contains the jupyter notebooks for the Mooc __Basics of Machine Learning__.
To set up your environment, we recommend using the [conda](https://docs.conda.io/en/latest/miniconda.html) package manager; you can use it to create an environment with `conda env create -f environment.yaml`.
To activate this environment, run `conda activate basicsml`.

